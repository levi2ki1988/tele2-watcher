'use strict';
const express = require('express');
const router = express.Router();
const request = require('request');
const os = require('os');

router.get('/', (req, res) => {
    var ifaces = os.networkInterfaces();
    var possibleIp = [];
    Object.keys(ifaces).forEach(function(value) {
        if (~value.search(/сет[ьи]/i)) {
            if (ifaces[value].length > 1) {
                ifaces[value].forEach(function(cv) {
                  cv.family === 'IPv4' ? possibleIp.push(cv.address) : null;
                })
            }
            value.family === 'IPv4' ? possibleIp.push(value.address) : null;
        }
    });

    res.render('tele2', {
        domain: req.query.domain,
        port: req.connection.localPort,
        ip: possibleIp,
    })
});

router.get('/:domain', (req, res) => {
    /**
     * @todo: response with link with external ip and params
     * */
});

router.get('/:domain/show', (req, res) => {
    request
        .get(`http://templater.kpmir.ru:8312/${req.params.domain}/redirect?serviceId=1&msisdn=79200307790&useSMSLink=true&forwardURL=http://google.com&provider_name=%D0%9E%D0%9E%D0%9E%20%D0%A2%20%D0%9C%D0%B5%D0%B4%D0%B8%D0%B0&reg_pmnt_cost=30%20%D1%80%D1%83%D0%B1.%20%D0%B2%20%D1%81%D1%83%D1%82%D0%BA%D0%B8&subscribe_cost=30%20%D1%80%D1%83%D0%B1.&period=%D1%81%D1%83%D1%82%D0%BA%D0%B8&sms=itskmagick`)
        .on('response', (response) => {
            response.setEncoding('utf-8');
            response.pipe(res)
        })
        .on('error', (err) => {
            console.log(err)
        })
});

module.exports = router;
