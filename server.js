'use strict';
/**
 * @docs https://github.com/nodejitsu/node-http-proxy
 * */
const url = require('url');
const http = require('http');
const request = require('request');
const os = require('os');
const color = require('colors/safe');

const server = http.createServer().listen(8008);
console.log(color.green('Server listen on 8008'));

server.on('request', (req, res) => {
    if (req.url === '/favicon.ico') return;
    console.log(url.parse(req.url));
    if (req.url !== '/') {
        let urlHost = req.url;
        request
            .get(`http://templater.kpmir.ru:8312${urlHost}/redirect?serviceId=1&msisdn=79200307790&useSMSLink=true&forwardURL=http://google.com&provider_name=%D0%9E%D0%9E%D0%9E%20%D0%A2%20%D0%9C%D0%B5%D0%B4%D0%B8%D0%B0&reg_pmnt_cost=30%20%D1%80%D1%83%D0%B1.%20%D0%B2%20%D1%81%D1%83%D1%82%D0%BA%D0%B8&subscribe_cost=30%20%D1%80%D1%83%D0%B1.&period=%D1%81%D1%83%D1%82%D0%BA%D0%B8&sms=itskmagick`)
            .on('response', (response) => {
                response.setEncoding('utf-8');
                response.pipe(res)
            })
            .on('error', (err) => {
                console.log(err)
            })
    } else {
        var ifaces = os.networkInterfaces();
        res.writeHead(200, {'Content-Type': 'text/plain; charset=utf-8'});
        Object.keys(ifaces).forEach((value) => {
            if (~value.search(/сет[ьи]/i)) {
                if (ifaces[value].length > 1) {
                    ifaces[value].forEach((cv) => {
                        cv.family === 'IPv4' ? res.write(`Ip вашего внешнего интерфейса "${value}" -- ${cv.address} \n`) : null;
                    })
                }
            }
            // res.write(`iface ${value} ip ${ifaces[value][0].address} \n`)
        });
        res.end();
    }
});

process.stdin.resume();
process.stdin.setEncoding('utf8');
console.log(color.green('Type "stop" for exit process'));
process.stdin.on('data', function (text) {
    if (text.includes('stop')) {
        done();
    }
});

function done() {
    console.log(color.yellow('Exit now'));
    server.close();
    process.exit();
}