'use strict';
window.onload = function () {
    var els = document.querySelectorAll('.copy');
    for (let i of els) {
        i.onclick = function () {
            return false
        };
        i.addEventListener('click', function (e) {
            let textToCopy = this.dataset.location;
            var elWrapper = document.createElement('p');
            var rng = document.createRange();

            elWrapper.innerHTML = textToCopy;
            elWrapper.style.cssText = 'position: fixed; top: -9999px; width: 1px; height: 1px; font-size: 1px';
            document.body.appendChild(elWrapper);
            rng.selectNode(elWrapper);
            var sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(rng);

            try {
                var successful = document.execCommand('copy');
                var msg = successful ? 'successful' : 'unsuccessful';
                console.log('Copying text command was ' + msg);
            } catch (err) {
                console.log('unable to copy');
            } finally {
                elWrapper.remove();
            }
        })
    }
};